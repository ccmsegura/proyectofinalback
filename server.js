var express = require('express')
var app = express()
var fs = require('fs')
var requestJson = require ('request-json')
var https = require('https')
var port = process.env.PORT || 3001
var bodyParser = require('body-parser')

https.createServer({
   key: fs.readFileSync('./ProyectoFinal.key'),
   cert: fs.readFileSync('./ProyectoFinal.crt')
}, app).listen(port, function() {
   console.log("Servidor HTTPs escuchando en el puerto " + port)
})

app.use(bodyParser.json())

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancocms/collections"
var apiKey = "apiKey=kJAwhCULtWcYjMPzIG1nEA00GH6wo4AA"
var clienteMlab = null

var urlCurrencyRaiz = "http://www.apilayer.net/api/"
var apiCurr = "access_key=907c8e316c075876d8ef2d40c9b2ca5b"
var clienteCurr = null

// app.listen(port)

// console.log("API escuchando en el puerto" + port)

app.get('/api/v1',function(req,res){
  res.send({"mensaje":"Bienvenido a mi API"})
})

app.get('/api/v1/usuarios', function(req,res){
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('',function(err,resM,body){
    if(!err)
      res.send(body)
  })
})

app.post('/api/v1/usuarios/login', function(req,res){
  var email = req.headers.email
  var password = req.headers.password
  var query = 'q={"email":"' + email + '","password":"' + password + '"}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1)
      {
        console.log(body)
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":true}}'
        clienteMlab.put ( '?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          console.log()
          res.send({"login":"ok", "id":body[0].id, "first_name":body[0].first_name, "last_name":body[0].last_name})})
      }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})


app.post('/api/v1/usuarios/logout', function(req,res){
  var id = req.headers.id
  console.log(id)
  var query = 'q={"id":' + id + ',"logged" : true }'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) //estaba logado
      {
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":false}}'
        clienteMlab.put ( '?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
          res.send({"logout":"ok", "id":body[0].id})})
      }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})


app.get('/api/v1/cuentas/usuario', function(req,res){
  var cliente = req.headers.idcliente
  var ibancliente = []
  var query = 'q={"idcliente":' + cliente + '}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      for (var i = 0; i < body.length; i++) {
        if(body[i].idcliente == cliente){
          ibancliente.push(body[i].iban)
          console.log(body[i])
        }
      }
        res.send(ibancliente)
    }
    else {
      res.status(404).send('Usuario no tiene cuentas')
    }
  })
})

app.get('/api/v1/movimientos', function(req,res){
  var iban = req.headers.iban
  var query = 'q={"iban": "' +  iban  + '"} &f={"movimientos":1, "_id":0, "saldo":1}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
          res.send(body)
      }
      else {
        res.status(404).send('Usuario no tiene cuenta')
      }
  })
})

app.post('/api/v1/altausuarios', function(req,res){

  var nuevo = {"first_name":req.headers.first_name, "last_name":req.headers.last_name,
    "email":req.headers.email, "gender":req.headers.gender, "password":req.headers.password,
    "country":req.headers.country}
  //clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios" + apiKey)
  var query = 'q={"first_name": "' + req.headers.first_name + '" , "email": "' + req.headers.email + '"}'
  var sort = 's={"id":-1}&f={"id":1, "_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&c=true&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (resM.body>=1){
        console.log("usuario existente")
      }
      else {
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + sort + "&l=1&" + apiKey)
        clienteMlab.get('', function(err, resM, body) {
          var max = parseInt(body[0].id)
          nuevo.id = max+1
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
          console.log(nuevo)
          clienteMlab.post('',nuevo,function(err, res, body) {
            console.log(nuevo)
          })
        })
      }
    }
    else {
        res.status(404).send('Faltan datos')
    }
  })
})

app.post('/api/v1/transferencias', function(req,res){
  var ibanorigen = req.headers.ibanorigen
  var ibandestino = req.headers.ibandestino
  var moneda = req.headers.moneda
  var cantidad = req.headers.cantidad
  var fecha = Date(req.headers.fecha)
  var qio = 'q={"iban":"' + ibanorigen + '"}'
  var qid = 'q={"iban":"' + ibandestino + '"}'
  // movimientos = []
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + qio + "&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    var saldoo = parseFloat(body[0].saldo)
    if (!err) {
      if (saldoo >= cantidad){
        console.log(saldoo)

        //Actualizar datos origen, movimientos y saldo
        //clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + qid + "&" + apiKey)
        //clienteMlab.get('', function(err, resM, body) {
          var ido = parseInt(body[0].movimientos.length)
          var nuevoido = ido+1
          var movimientoo = {"id":nuevoido,"fecha":req.headers.fecha,"importe":req.headers.cantidad,"moneda":req.headers.moneda}
          console.log("movimiento origen", movimientoo)
          console.log("body[0].movimientos", body[0].movimientos)
          body[0].movimientos.push(movimientoo)
          console.log("movimiento origen con cambio", body[0].movimientos)
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + qio + "&" + apiKey)
          var queryo = 'q={"iban":"' + ibanorigen + '"} &f={"movimientos":1, "_id":0}'
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + queryo + "&" + apiKey)
          var movimientoso = {"$set":{"movimientos": body[0].movimientos}}
          clienteMlab.put('', movimientoso, function(err, res, body) {
            console.log("movimientos actualizados")
          })

          if (!err) {
            var saldoto = saldoo - parseFloat(cantidad)
            console.log(saldoto)
            var cambioo = {"$set":{"saldo": saldoto}}
            console.log(cambioo)
            clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + qio + "&" + apiKey)
            clienteMlab.put('', cambioo, function(err, resP, body) {
              console.log("transferencia realizada")
            })
          }

      //Actualizar datos destino, movimientos y saldo
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + qid + "&" + apiKey)
        clienteMlab.get('', function(err, resM, body) {
          var id = parseInt(body[0].movimientos.length)
          var nuevoid = id+1
          var movimiento = {"id":nuevoid,"fecha":req.headers.fecha,"importe":req.headers.cantidad,"moneda":req.headers.moneda}
          console.log("movimiento", movimiento)
          console.log("body[0].movimientos", body[0].movimientos)
          body[0].movimientos.push(movimiento)
          console.log("movimiento destino con cambio", body[0].movimientos)
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + qid + "&" + apiKey)
          var query = 'q={"iban":"' + ibandestino + '"} &f={"movimientos":1, "_id":0}'
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + apiKey)
          var movimientosd = {"$set":{"movimientos": body[0].movimientos}}
          clienteMlab.put('', movimientosd, function(err, res, body) {
            console.log("movimientos actualizados")
          })
          var saldod = parseFloat(body[0].saldo)
          if (!err) {
            var saldot = parseFloat(cantidad) + saldod
            console.log(saldot)
            var cambio = {"$set":{"saldo": saldot}}
            console.log(cambio)
            clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + qid + "&" + apiKey)
            clienteMlab.put('', cambio, function(err, resP, body) {
              console.log("transferencia realizada")
              res.status(200).send('transferencia realizada')
            })
          }
          else {
            res.status(404).send('No encontrado cliente destino')
          }
        })
      }
      else {
        res.status(404).send('No hay suficiente saldo')
      }
    }
  })
})

app.delete('/api/v1/usuarios', function(req,res){

  var query = 'q={"password": "' + req.headers.password + '"}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&" + apiKey)
  clienteMlab.get('', function(err, res, body) {
    if (!err) {
        console.log("usuario existente")
        var usdel = body[0]._id.$oid
        console.log(usdel)
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios/" + usdel + "?" + apiKey)
        console.log(clienteMlab)
        clienteMlab.delete('', function(err, res, body) {
          console.log("usuario borrado")
        })
     }
    else {
        res.status(404).send('error')
    }
  })
})

app.get('/api/v1/currency', function(req,res){
  var query = req.headers.currency
  clienteCurr = requestJson.createClient(urlCurrencyRaiz + "/live?" + "currencies=" + query + "&" + apiCurr)
  console.log(urlCurrencyRaiz + "/live?" + "currencies=" + query + "&" + apiCurr)
  clienteCurr.get('', function(err, resM, body) {
    if (!err) {
          res.send(body.quotes)
          console.log(body.quotes)
      }
      else {
        res.status(404).send('Moneda no encontrada')
      }
  })
})
