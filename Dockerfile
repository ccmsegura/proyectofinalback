# Imagen raíz
from node
# Carpeta raíz
WORKDIR /api
# Copia de archivos
ADD . /api
# Instalo los paquetes necesarios
RUN npm install
# Volumen de la imagen
VOLUME ["/logs"]
# Puerto que expone
EXPOSE 3001
# Comando de iniciado
CMD ["npm", "start"]
