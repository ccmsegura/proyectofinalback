var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../server') //para que arrancaque nuestra API y ejecute lo que hay

var should = chai.should() //además de la prueba indica que debería ser así.

chai.use(chaiHttp) //Configurar chai con modulo http

describe('Tests de conectividad', () => {
  it ('Google funciona', (done) => {
    chai.request('http://www.google.es').get('/').end((err,res) => {
      res.should.have.status(200)
      done()
    })
  })
})

describe('Test de la API', () => {
  it ('Raíz OK', (done) => {
    chai.request('http://localhost:3001').get('/api/v1').end((err,res) => {
      res.should.have.status(200)
      res.body.mensaje.should.be.eql("Bienvenido a mi API")
      done()
    })
  })
  it ('Lista de usuarios', (done) => {
    chai.request('http://localhost:3001').get('/api/v1/usuarios').end((err,res) => {
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array') //chequea el body devuleve un tipo array
      for (var i = 0; i <res.body.length; i++) {
        res.body[i].should.have.property('id')
        res.body[i].should.not.have.property('password')
      }
      done()
    })
  })
})
